<?php

declare(strict_types=1);

namespace RPGBundle\Handler;

use RPGBundle\Command\TurnCommandInterface;
use RPGBundle\Command\CommandInterface;
use RPGBundle\Event\PostPlayerTurnEvent;
use RPGBundle\Service\World\WorldContextInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Dispatch an event after Player takes action considered as turn
 */
class DispatchEventAfterPlayerTurn implements HandlerInterface
{
    /**
     * @var HandlerInterface
     */
    protected $decorated;

    /**
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;

    /**
     * @var WorldContextInterface
     */
    protected $worldContext;

    public function __construct(
        HandlerInterface $decorated,
        EventDispatcherInterface $eventDispatcher,
        WorldContextInterface $worldContext
    ) {
        $this->decorated = $decorated;
        $this->eventDispatcher = $eventDispatcher;
        $this->worldContext = $worldContext;
    }

    /**
     * @inheritdoc
     */
    public function handle(CommandInterface $command)
    {
        $result = $this->decorated->handle($command);

        if ($command instanceof TurnCommandInterface
            && $command->getCharacter() === $this->worldContext->getPlayer()
        ) {
            $event = new PostPlayerTurnEvent($command, $this->worldContext->getPlayer());
            $this->eventDispatcher->dispatch(PostPlayerTurnEvent::NAME, $event);
        }

        return $result;
    }
}
