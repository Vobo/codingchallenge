<?php

declare(strict_types=1);

namespace RPGBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use RPGBundle\Command\CommandInterface;
use RPGBundle\Command\LevelUp;
use RPGBundle\Service\Player\LevelUpVoter\LevelUpVoterInterface;

class PlayerLevelUpHandler implements HandlerInterface
{
    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @var LevelUpVoterInterface
     */
    protected $levelUpVoter;

    public function __construct(
        ObjectManager $objectManager,
        LevelUpVoterInterface $levelUpVoter
    ) {
        $this->objectManager = $objectManager;
        $this->levelUpVoter = $levelUpVoter;
    }

    /**
     * @inheritdoc
     */
    public function handle(CommandInterface $levelUp)
    {
        /** @var LevelUp $levelUp */
        if (!$levelUp instanceof LevelUp) {
            throw new \InvalidArgumentException('Expected PlayerLevelUp, got ' . get_class($levelUp));
        }

        $player = $levelUp->getPlayer();
        $requiredXP = $this->levelUpVoter->requiredXP($player);

        if ($requiredXP > $player->getXP()) {
            throw new InvalidActionException('XP too low');
        }

        $player
            ->setXP($player->getXP() - $requiredXP)
            ->setLevel($player->getLevel() + 1);

        // @todo refactor with strategies
        switch ($levelUp->getImprove()) {
            case LevelUp::IMPROVE_HEALTH:
                $newHealth = $player->getHealth() * 3 / 2;
                $player->setHealth((int) $newHealth);
                break;

            case LevelUp::IMPROVE_POWER:
                $newPower = $player->getPower() * 3 / 2;
                $player->setPower((int) $newPower);
                break;

            default:
                throw new InvalidActionException(sprintf(
                    'Leveling up %s is not supported',
                    $levelUp->getImprove()
                ));
        }

        $this->objectManager->persist($player);
        $this->objectManager->flush();

        return $player;
    }
}
