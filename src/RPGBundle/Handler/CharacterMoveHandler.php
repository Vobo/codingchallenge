<?php

declare(strict_types=1);

namespace RPGBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use RPGBundle\Command\CharacterMove;
use RPGBundle\Command\CommandInterface;
use RPGBundle\Service\World\WorldContextInterface;

class CharacterMoveHandler implements HandlerInterface
{
    /**
     * @var WorldContextInterface
     */
    protected $worldContext;

    /**
     * @var ObjectManager
     */
    protected $objectManager;

    public function __construct(
        WorldContextInterface $worldContext,
        ObjectManager $objectManager
    ) {
        $this->worldContext = $worldContext;
        $this->objectManager = $objectManager;
    }

    /**
     * @inheritdoc
     */
    public function handle(CommandInterface $characterMove)
    {
        /** @var CharacterMove $characterMove */
        if (!$characterMove instanceof CharacterMove) {
            throw new \InvalidArgumentException('Expected CharacterMove, got ' . get_class($characterMove));
        }

        $world = $this->worldContext->getWorld();
        $position = $characterMove->getPosition();
        $character = $characterMove->getCharacter();

        if ($world->hasCharacter($position)) {
            throw new InvalidActionException('Position taken');
        }

        // @todo replace with more complex logic
        if ($character->getPosition()->distance($position) > 1) {
            throw new InvalidActionException('That\'s too far');
        }

        if ($position->getX() >= $world->getWidth()
            || $position->getX() < 1
            || $position->getY() >= $world->getHeight()
            || $position->getY() < 1
        ) {
            throw new InvalidActionException('Out of bounds');
        }

        $character->setPosition($position);

        $this->objectManager->persist($character);
        $this->objectManager->flush();

        return $character;
    }
}