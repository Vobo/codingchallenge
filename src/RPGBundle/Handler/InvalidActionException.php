<?php

declare(strict_types=1);

namespace RPGBundle\Handler;

class InvalidActionException extends \Exception
{ }