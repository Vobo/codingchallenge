<?php

declare(strict_types=1);

namespace RPGBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use RPGBundle\Command\CommandInterface;
use RPGBundle\Command\NewGame;
use RPGBundle\Service\Mob\MobFactoryInterface;
use RPGBundle\Service\Player\PlayerFactoryInterface;
use RPGBundle\Service\World\WorldBuilderFactoryInterface ;

class NewGameHandler implements HandlerInterface
{
    const MAP_PERCENTAGE_FOR_MOBS = 10;

    /**
     * @var PlayerFactoryInterface
     */
    protected $playerFactory;

    /**
     * @var WorldBuilderFactoryInterface
     */
    protected $worldBuilderFactory;

    /**
     * @var MobFactoryInterface
     */
    protected $mobFactory;

    /**
     * @var ObjectManager
     */
    protected $objectManager;

    public function __construct(
        PlayerFactoryInterface $playerFactory,
        WorldBuilderFactoryInterface $worldBuilderFactory,
        MobFactoryInterface $mobFactory,
        ObjectManager $objectManager
    ) {
        $this->playerFactory = $playerFactory;
        $this->worldBuilderFactory = $worldBuilderFactory;
        $this->mobFactory = $mobFactory;
        $this->objectManager = $objectManager;
    }

    /**
     * @inheritdoc
     */
    public function handle(CommandInterface $newGame)
    {
        /** @var NewGame $newGame */
        if (!$newGame instanceof NewGame) {
            throw new \InvalidArgumentException('Expected NewGame, got ' . get_class($newGame));
        }

        $builder = $this->worldBuilderFactory->create();

        $player = $this->playerFactory->createPlayer($newGame->getName(), $newGame->getClass());
        $builder
            ->setDimensions($newGame->getWidth(), $newGame->getHeight())
            ->addPlayer($player);

        $count = $this->calculateNumberOfMobs($newGame);

        for ($i = 0; $i < $count; $i++) {
            $builder->addMob($this->mobFactory->createMob());
        }

        $world = $builder->getWorld();

        $this->objectManager->persist($world);
        $this->objectManager->flush();

        return $world;
    }

    /**
     * @param NewGame $newGame
     * @return int
     */
    protected function calculateNumberOfMobs(NewGame $newGame) : int
    {
        $mapSize = $newGame->getWidth() * $newGame->getHeight();
        $mobs = $mapSize * self::MAP_PERCENTAGE_FOR_MOBS / 100;

        return (int) $mobs;
    }
}