<?php

declare(strict_types=1);

namespace RPGBundle\Handler;

use RPGBundle\Command\CommandInterface;
use RPGBundle\Event\WorldLoadedEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class EventDispatchingNewGameHandler implements HandlerInterface
{
    protected $wrapped;

    protected $eventDispatcher;

    public function __construct(
        NewGameHandler $decorated,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->wrapped = $decorated;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @inheritdoc
     */
    public function handle(CommandInterface $command)
    {
        $world = $this->wrapped->handle($command);
        $this->eventDispatcher->dispatch(WorldLoadedEvent::NAME, new WorldLoadedEvent($world));

        return $world;
    }
}