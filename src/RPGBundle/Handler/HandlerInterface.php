<?php

declare(strict_types=1);

namespace RPGBundle\Handler;

use RPGBundle\Command\CommandInterface;

interface HandlerInterface
{
    /**
     * @param CommandInterface $command
     * @return mixed Command result
     */
    public function handle(CommandInterface $command);
}