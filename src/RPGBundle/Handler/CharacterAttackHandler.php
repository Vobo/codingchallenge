<?php

declare(strict_types=1);

namespace RPGBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use RPGBundle\Command\CharacterAttack;
use RPGBundle\Command\CommandInterface;
use RPGBundle\Entity\Character;
use RPGBundle\Event\CharacterDiedEvent;
use RPGBundle\Model\AttackResult;
use RPGBundle\Service\Character\XP\XPGeneratorInterface;
use RPGBundle\Service\World\WorldContextInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CharacterAttackHandler implements HandlerInterface
{
    /**
     * @var WorldContextInterface
     */
    protected $worldContext;

    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;

    /**
     * @var XPGeneratorInterface
     */
    protected $xpGenerator;

    public function __construct(
        WorldContextInterface $worldContext,
        ObjectManager $objectManager,
        EventDispatcherInterface $eventDispatcher,
        XPGeneratorInterface $xpGenerator
    ) {
        $this->worldContext = $worldContext;
        $this->objectManager = $objectManager;
        $this->eventDispatcher = $eventDispatcher;
        $this->xpGenerator = $xpGenerator;
    }

    /**
     * @param CommandInterface $characterAttack
     * @return AttackResult
     * @throws InvalidActionException
     * @throws \Exception
     */
    public function handle(CommandInterface $characterAttack)
    {
        /** @var CharacterAttack $characterAttack */
        if (!$characterAttack instanceof CharacterAttack) {
            throw new \InvalidArgumentException('Expected CharacterAttack, got ' . get_class($characterAttack));
        }

        $world = $this->worldContext->getWorld();
        $player = $this->worldContext->getPlayer();
        $attacker = $characterAttack->getCharacter();
        $target = $characterAttack->getTarget();

        if (!$world->hasCharacter($target)) {
            throw new InvalidActionException('No one to attack');
        }

        $defender = $this->worldContext->get($target);

        if ($attacker === $defender) {
            throw new InvalidActionException('Cannot attack yourself');
        }

        if ($defender->isDead()) {
            throw new InvalidActionException('Target already dead');
        }

        // @todo refactor with strategies
        $damage = $attacker->getPower() + ($attacker->getLevel() - $defender->getLevel()) * 2;
        $damage = min($damage, $defender->getHealth()); // Can't take more health than defender has

        $defender->setHealth($defender->getHealth() - $damage);
        $grantedXp = $this->grantXP($defender);

        // Remove dead NPCs and Mobs, but leave player
        ($defender->isDead() && $defender !== $player)
            ? $this->objectManager->remove($defender)
            : $this->objectManager->persist($defender);

        $this->objectManager->persist($player);
        $this->objectManager->flush();

        if ($defender->isDead()) {
            $event = new CharacterDiedEvent($defender);
            $this->eventDispatcher->dispatch(CharacterDiedEvent::NAME, $event);
        }

        return new AttackResult(
            $attacker,
            $defender,
            $damage,
            $grantedXp
        );
    }

    /**
     * @param Character $defender
     * @return int Granted XP
     * @throws \Exception
     */
    protected function grantXP(Character $defender) : int
    {
        $player = $this->worldContext->getPlayer();

        if (!$defender->isDead() || $defender === $player) {
            return 0;
        }

        $xp = $this->xpGenerator->generateXP($defender);
        $player->setXP($player->getXP() + $xp);
        $this->objectManager->persist($player);

        return $xp;
    }
}
