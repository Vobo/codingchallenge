<?php

declare(strict_types=1);

namespace RPGBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\Model;
use RPGBundle\Entity\Item;
use Swagger\Annotations as SWG;

class ItemController extends FOSRestController
{
    /**
     * Equip item
     *
     * @Rest\View()
     * @Rest\Post("/item/{item}/equip")
     *
     * @SWG\Tag(name="Item")
     * @SWG\Parameter(
     *     name="item",
     *     in="path",
     *     required=true,
     *     type="integer",
     *     description="Item id"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Returns the Item",
     *     @Model(type=Item::class)
     * )
     *
     * @param Item
     * @return Item
     */
    public function postItemEquipAction(Item $item)
    {
        // @todo
    }

    /**
     * Unequip item
     *
     * @Rest\View()
     * @Rest\Post("/item/{item}/unequip")
     *
     * @SWG\Tag(name="Item")
     * @SWG\Parameter(
     *     name="item",
     *     in="path",
     *     required=true,
     *     type="integer",
     *     description="Item id"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Returns the Item",
     *     @Model(type=Item::class)
     * )
     *
     * @param Item
     * @return Item
     */
    public function postItemUnequipAction(Item $item)
    {
        // @todo
    }

    /**
     * Use item
     *
     * @Rest\View()
     * @Rest\Post("/item/{item}/use")
     *
     * @SWG\Tag(name="Item")
     * @SWG\Parameter(
     *     name="item",
     *     in="path",
     *     required=true,
     *     type="integer",
     *     description="Item id"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Returns the Item",
     *     @Model(type=Item::class)
     * )
     *
     * @param Item
     * @return Item
     */
    public function postItemUseAction(Item $item)
    {
        // @todo
    }
}