<?php

declare(strict_types=1);

namespace RPGBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use RPGBundle\Entity\Save;
use RPGBundle\Entity\World;
use RPGBundle\Command\NewGame;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Swagger\Annotations as SWG;

class GameController extends FOSRestController
{
    /**
     * Start new game
     *
     * @Rest\Post("/game")
     * @Rest\View()
     *
     * @SWG\Post(
     *     tags={"Game"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          name="newGame",
     *          in="body",
     *          type="json",
     *          description="New game options",
     *          required=true,
     *          @Model(type=NewGame::class)
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Returns the generated world",
     *         @Model(type=World::class)
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Validation error"
     *     )
     * )
     *
     * @ParamConverter("newGame", converter="fos_rest.request_body")
     * @param NewGame $newGame
     * @param ConstraintViolationListInterface $validationErrors
     * @return World|View
     * @throws \Exception
     */
    public function postGameAction(NewGame $newGame, ConstraintViolationListInterface $validationErrors)
    {
        if ($validationErrors->count()) {
            return $this->view($validationErrors, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return $this->get('rpg.bus')->dispatch($newGame);
    }

    /**
     * Show current game
     *
     * @Rest\Get("/game/{world}")
     * @Rest\View()
     *
     * @SWG\Get(
     *     tags={"Game"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="world",
     *         in="path",
     *         required=true,
     *         type="integer",
     *         description="World id"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Returns the world",
     *         @Model(type=World::class)
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="World not found"
     *     )
     * )
     *
     * @param World $world
     * @return World
     */
    public function getGameAction(World $world)
    {
        return $world;
    }

    /**
     * Save game
     *
     * @Rest\Post("/game/{world}/save")
     * @Rest\View()
     *
     * @SWG\Post(
     *     tags={"Game"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="world",
     *         in="path",
     *         required=true,
     *         type="integer",
     *         description="World id"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Returns the Save",
     *         @Model(type=Save::class)
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="World not found"
     *     )
     * )
     *
     * @return Save
     */
    public function postGameSaveAction(World $world)
    {
        // @todo
    }

    /**
     * Load game
     *
     * @Rest\Post("/game/{world}/load")
     * @Rest\View()
     *
     * @SWG\Post(
     *     tags={"Game"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="world",
     *         in="path",
     *         required=true,
     *         type="integer",
     *         description="World id"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Returns the Save",
     *         @Model(type=Save::class)
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="World not found"
     *     )
     * )
     *
     * @return Save
     */
    public function postGameLoadAction()
    {
        // @todo
    }
}