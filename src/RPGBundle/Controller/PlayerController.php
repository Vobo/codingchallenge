<?php

declare(strict_types=1);

namespace RPGBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use RPGBundle\Command\CharacterAttack;
use RPGBundle\Command\CharacterMove;
use RPGBundle\Command\LevelUp;
use RPGBundle\Entity\Player;
use RPGBundle\Entity\Position;
use RPGBundle\Entity\World;
use RPGBundle\Handler\InvalidActionException;
use RPGBundle\Model\AttackResult;
use Nelmio\ApiDocBundle\Annotation\Model;
use RPGBundle\Model\ValidationError;
use Swagger\Annotations as SWG;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class PlayerController extends FOSRestController
{
    /**
     * Info about a player
     *
     * @Rest\Get("/game/{world}/player")
     * @Rest\View()
     *
     * @SWG\Get(
     *     tags={"Player"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="world",
     *         in="path",
     *         required=true,
     *         type="integer",
     *         description="World id"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Returns the Player",
     *         @Model(type=Player::class)
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="World not found"
     *     )
     * )
     *
     * @param World $world
     * @return Player
     * @throws \Exception
     */
    public function getPlayerAction(World $world)
    {
        return $this->get('rpg.world.context')->getPlayer();
    }

    /**
     * Level up player
     *
     * @Rest\Post("/game/{world}/player/level-up")
     * @Rest\View()
     *
     * @SWG\Post(
     *     tags={"Player"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="world",
     *         in="path",
     *         required=true,
     *         type="integer",
     *         description="World id"
     *     ),
     *     @SWG\Parameter(
     *          name="levelUp",
     *          in="body",
     *          type="json",
     *          description="Level up",
     *          required=true,
     *          @Model(type=LevelUp::class)
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Returns the Player",
     *         @Model(type=Player::class)
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Validation error"
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="World not found"
     *     )
     * )
     *
     * @ParamConverter("levelUp", converter="fos_rest.request_body")
     *
     * @param World $world
     * @param LevelUp $levelUp
     * @param ConstraintViolationListInterface $validationErrors
     * @return Player|View
     * @throws \Exception
     */
    public function postPlayerLevelUpAction(World $world, LevelUp $levelUp, ConstraintViolationListInterface $validationErrors)
    {
        if ($validationErrors->count()) {
            return $this->view($validationErrors, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $player = $this->get('rpg.world.context')->getPlayer();
        $levelUp->setPlayer($player);

        try {
            if ($player->isDead()) {
                throw new InvalidActionException('Player is dead');
            }

            return $this->get('rpg.bus')->dispatch($levelUp);
        } catch (InvalidActionException $e) {
            return (new ValidationError($e->getMessage()))->toView();
        }
    }

    /**
     * Move player
     *
     * @Rest\Post("/game/{world}/player/move")
     * @Rest\View()
     *
     * @SWG\Post(
     *     tags={"Player"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="world",
     *         in="path",
     *         required=true,
     *         type="integer",
     *         description="World id"
     *     ),
     *     @SWG\Parameter(
     *          name="position",
     *          in="body",
     *          type="json",
     *          description="Move",
     *          required=true,
     *          @Model(type=Position::class)
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Returns the Player",
     *         @Model(type=Player::class)
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Validation error"
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="World not found"
     *     )
     * )
     *
     * @ParamConverter("position", converter="fos_rest.request_body")
     *
     * @param World $world
     * @param Position $position
     * @param ConstraintViolationListInterface $validationErrors
     * @return Player|View
     * @throws \Exception
     */
    public function postPlayerMoveAction(World $world, Position $position, ConstraintViolationListInterface $validationErrors)
    {
        if ($validationErrors->count()) {
            return $this->view($validationErrors, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $player = $this->get('rpg.world.context')->getPlayer();
        $characterMove = new CharacterMove($player, $position);

        try {
            if ($player->isDead()) {
                throw new InvalidActionException('Player is dead');
            }

            return $this->get('rpg.bus')->dispatch($characterMove);
        } catch (InvalidActionException $e) {
            return (new ValidationError($e->getMessage()))->toView();
        }
    }

    /**
     * Attack
     *
     * @Rest\Post("/game/{world}/player/attack")
     * @Rest\View()
     *
     * @SWG\Post(
     *     tags={"Player"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="world",
     *         in="path",
     *         required=true,
     *         type="integer",
     *         description="World id"
     *     ),
     *     @SWG\Parameter(
     *          name="position",
     *          in="body",
     *          type="json",
     *          description="Attack",
     *          required=true,
     *          @Model(type=Position::class)
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Returns the Player",
     *         @Model(type=AttackResult::class)
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Validation error"
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="World not found"
     *     )
     * )
     *
     * @ParamConverter("position", converter="fos_rest.request_body")
     *
     * @param World $world
     * @param Position $position
     * @param ConstraintViolationListInterface $validationErrors
     * @return Player|View
     * @throws \Exception
     */
    public function postPlayerAttackAction(World $world, Position $position, ConstraintViolationListInterface $validationErrors)
    {
        if ($validationErrors->count()) {
            return $this->view($validationErrors, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $player = $this->get('rpg.world.context')->getPlayer();
        $characterAttack = new CharacterAttack($player, $position);

        try {
            if ($player->isDead()) {
                throw new InvalidActionException('Player is dead');
            }

            return $this->get('rpg.bus')->dispatch($characterAttack);
        } catch (InvalidActionException $e) {
            return (new ValidationError($e->getMessage()))->toView();
        }
    }
}