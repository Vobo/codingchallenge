<?php

declare(strict_types=1);

namespace RPGBundle\Listener;

use RPGBundle\Entity\World;
use RPGBundle\Service\World\SetWorldContextInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerArgumentsEvent;

class RouterWorldContextListener
{
    /**
     * @var SetWorldContextInterface
     */
    protected $worldContext;

    public function __construct(
        SetWorldContextInterface $worldContext
    ) {
        $this->worldContext = $worldContext;
    }

    public function onKernelController(FilterControllerArgumentsEvent $event)
    {
        $arguments = $event->getArguments();

        foreach ($arguments as $argument) {
            if ($argument instanceof World) {
                $this->worldContext->setWorld($argument);

                return;
            }
        }
    }
}