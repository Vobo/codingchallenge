<?php

declare(strict_types=1);

namespace RPGBundle\Listener;

use RPGBundle\Event\WorldLoadedEvent;
use RPGBundle\Service\World\SetWorldContextInterface;

class NewWorldContextListener
{
    /**
     * @var SetWorldContextInterface
     */
    protected $worldContext;

    public function __construct(
        SetWorldContextInterface $worldContext
    ) {
        $this->worldContext = $worldContext;
    }

    /**
     * Event listener
     *
     * @param WorldLoadedEvent $event
     */
    public function onWorldLoad(WorldLoadedEvent $event)
    {
        $this->worldContext->setWorld($event->getWorld());
    }
}
