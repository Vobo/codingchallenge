<?php

declare(strict_types=1);

namespace RPGBundle\Listener;

use RPGBundle\Event\PostPlayerTurnEvent;
use RPGBundle\Service\Mob\AI\AIInterface;
use RPGBundle\Service\World\WorldContextInterface;

class MobsTurnListener
{
    /**
     * @var WorldContextInterface
     */
    protected $worldContext;

    /**
     * @var AIInterface
     */
    protected $mobAI;

    public function __construct(
        WorldContextInterface $worldContext,
        AIInterface $mobAI
    ) {
        $this->worldContext = $worldContext;
        $this->mobAI = $mobAI;
    }

    /**
     * Listener
     *
     * @param PostPlayerTurnEvent $event
     * @return void
     * @throws \Exception
     */
    public function postPlayerTurn(PostPlayerTurnEvent $event)
    {
        foreach ($this->worldContext->getMobs() as $mob) {
            $this->mobAI->takeAction($mob);
        }
    }
}