<?php

declare(strict_types=1);

namespace RPGBundle\Listener;

use Doctrine\Common\Persistence\ObjectManager;
use RPGBundle\Entity\Mob;
use RPGBundle\Event\CharacterDiedEvent;
use RPGBundle\Service\Mob\MobFactoryInterface;
use RPGBundle\Service\World\WorldBuilderFactoryInterface;
use RPGBundle\Service\World\WorldContextInterface;

class SpawnNewMobOnMobDeathListener
{
    /**
     * @var WorldContextInterface
     */
    protected $worldContext;

    /**
     * @var MobFactoryInterface
     */
    protected $mobFactory;

    /**
     * @var WorldBuilderFactoryInterface
     */
    protected $worldBuilderFactory;

    /**
     * @var ObjectManager
     */
    protected $objectManager;

    public function __construct(
        WorldContextInterface $worldContext,
        MobFactoryInterface $mobFactory,
        WorldBuilderFactoryInterface $worldBuilderFactory,
        ObjectManager $objectManager
    ) {
        $this->worldContext = $worldContext;
        $this->mobFactory = $mobFactory;
        $this->worldBuilderFactory = $worldBuilderFactory;
        $this->objectManager = $objectManager;
    }

    /**
     * Listener
     *
     * @param CharacterDiedEvent $event
     * @throws \Exception
     */
    public function onMobDeath(CharacterDiedEvent $event)
    {
        $character = $event->getDeadCharacter();

        if (!$character instanceof Mob) {
            return; // Spawn new Mob only once another one died
        }

        $world = $this->worldContext->getWorld();
        $mob = $this->mobFactory->createMob();
        $builder = $this->worldBuilderFactory->createForWorld($world);
        $builder->addMob($mob);
        $world = $builder->getWorld();

        $this->objectManager->persist($world);
        $this->objectManager->flush();
    }
}