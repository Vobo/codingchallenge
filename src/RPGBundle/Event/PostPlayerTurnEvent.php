<?php

declare(strict_types=1);

namespace RPGBundle\Event;

use RPGBundle\Command\CommandInterface;
use RPGBundle\Entity\Player;
use Symfony\Component\EventDispatcher\Event;

class PostPlayerTurnEvent extends Event
{
    const NAME = 'rpg.player.post_turn';

    /**
     * @var CommandInterface
     */
    protected $command;

    /**
     * @var Player
     */
    protected $player;

    public function __construct(CommandInterface $initialCommand, Player $player)
    {
        $this->command = $initialCommand;
        $this->player = $player;
    }

    /**
     * @return CommandInterface
     */
    public function getCommand() : CommandInterface
    {
        return $this->command;
    }

    /**
     * @return Player
     */
    public function getPlayer() : Player
    {
        return $this->player;
    }
}