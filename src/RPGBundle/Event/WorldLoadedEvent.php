<?php

declare(strict_types=1);

namespace RPGBundle\Event;

use RPGBundle\Entity\World;
use Symfony\Component\EventDispatcher\Event;

class WorldLoadedEvent extends Event
{
    const NAME = 'rpg.world.loaded';

    /**
     * @var World
     */
    protected $world;

    public function __construct(World $world)
    {
        $this->world = $world;
    }

    /**
     * @return World
     */
    public function getWorld() : World
    {
        return $this->world;
    }
}