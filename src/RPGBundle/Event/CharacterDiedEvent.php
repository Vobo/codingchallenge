<?php

declare(strict_types=1);

namespace RPGBundle\Event;

use RPGBundle\Entity\Character;
use Symfony\Component\EventDispatcher\Event;

class CharacterDiedEvent extends Event
{
    const NAME = 'rpg.character.died';

    /**
     * @var Character
     */
    protected $character;

    public function __construct(Character $deadCharacter)
    {
        $this->character = $deadCharacter;
    }

    /**
     * @return Character
     */
    public function getDeadCharacter() : Character
    {
        return $this->character;
    }
}