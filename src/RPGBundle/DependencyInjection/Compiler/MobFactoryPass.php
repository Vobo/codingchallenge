<?php

declare(strict_types=1);

namespace RPGBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

class MobFactoryPass implements CompilerPassInterface
{
    const SERVICE_ID = 'rpg.mob.factory';
    const TAG_MOB_PROTOTYPE = 'rpg.mob.prototype';
    const TAG_MODIFIER = 'rpg.mob.modifier';
    const PARAM_PRIORITY = 'priority';

    /**
     * @inheritdoc
     */
    public function process(ContainerBuilder $container)
    {
        // check if the primary service is defined
        if (!$container->has(self::SERVICE_ID)) {
            return;
        }

        $definition = $container->findDefinition(self::SERVICE_ID);
        $this
            ->registerPrototypes($definition, $container)
            ->registerModifiers($definition, $container);
    }

    /**
     * @param Definition $definition
     * @param ContainerBuilder $container
     * @return $this
     */
    protected function registerPrototypes(Definition $definition, ContainerBuilder $container)
    {
        // find all service IDs with the mob prototype tag
        $prototypeServices = $container->findTaggedServiceIds(self::TAG_MOB_PROTOTYPE);

        foreach ($prototypeServices as $id => $tags) {
            // add the prototype to the MobFactory service
            $definition->addMethodCall('registerPrototype', array(new Reference($id)));
        }

        return $this;
    }

    /**
     * @param Definition $definition
     * @param ContainerBuilder $container
     * @return $this
     */
    protected function registerModifiers(Definition $definition, ContainerBuilder $container)
    {
        // find all service IDs with the mob prototype tag
        $modifierServices = $container->findTaggedServiceIds(self::TAG_MODIFIER);
        // 2-d array - 1st dimension is priority, 2nd one contains modifiers in random order
        $sortedByPriority = [];

        foreach ($modifierServices as $id => $tags) {
            foreach ($tags as $attributes) {
                $priority = $attributes[self::PARAM_PRIORITY] ?? 0;

                if (!array_key_exists($priority, $sortedByPriority)) {
                    $sortedByPriority[$priority] = [];
                }

                $sortedByPriority[$priority][] = $id;
            }
        }

        krsort($sortedByPriority);

        foreach ($sortedByPriority as $services) {
            foreach ($services as $id) {
                // add the modifier to the MobFactory service
                $definition->addMethodCall('registerModifier', array(new Reference($id)));
            }
        }

        return $this;
    }
}