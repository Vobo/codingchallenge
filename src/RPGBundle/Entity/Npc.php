<?php

namespace RPGBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity()
 *
 * @Serializer\ExclusionPolicy("all")
 */
class Npc extends Character
{

}
