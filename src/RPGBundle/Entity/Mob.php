<?php

declare(strict_types=1);

namespace RPGBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Table(name="mob")
 * @ORM\Entity()
 *
 * @Serializer\ExclusionPolicy("all")
 */
class Mob extends Character
{ }
