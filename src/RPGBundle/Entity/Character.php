<?php

declare(strict_types=1);

namespace RPGBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Table(name="character_base")
 * @ORM\Entity()
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"mob" = "Mob", "player" = "Player", "npc" = "Npc"})
 *
 * @Serializer\ExclusionPolicy("all")
 */
abstract class Character
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\Expose
     */
    protected $id;

    /**
     * @var World
     *
     * @ORM\ManyToOne(targetEntity="World", inversedBy="characters")
     * @ORM\JoinColumn(name="world_id", referencedColumnName="id")
     */
    protected $world;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     *
     * @Serializer\Expose
     */
    protected $name = '';

    /**
     * @var int
     *
     * @ORM\Column(name="level", type="integer")
     *
     * @Serializer\Expose
     */
    protected $level = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="health", type="integer")
     *
     * @Serializer\Expose
     */
    protected $health = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="power", type="integer")
     *
     * @Serializer\Expose
     */
    protected $power = 0;

    /**
     * @var Position
     *
     * @ORM\Embedded(class="Position")
     *
     * @Serializer\Expose
     * @Serializer\Type("RPGBundle\Entity\Position")
     */
    protected $position;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param World $world
     * @return $this
     */
    public function setWorld(World $world)
    {
        $this->world = $world;

        return $this;
    }

    /**
     * @return World
     */
    public function getWorld() : World
    {
        return $this->world;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param integer $level
     * @return $this
     */
    public function setLevel(int $level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * @return int
     */
    public function getLevel() : int
    {
        return $this->level;
    }

    /**
     * @param integer $health
     * @return $this
     */
    public function setHealth(int $health)
    {
        $this->health = $health;

        return $this;
    }

    /**
     * @return int
     */
    public function getHealth() : int
    {
        return $this->health;
    }

    /**
     * @return bool
     */
    public function isDead() : bool
    {
        return 0 === $this->health;
    }

    /**
     * @return int
     */
    public function getPower(): int
    {
        return $this->power;
    }

    /**
     * @param int $power
     * @return $this
     */
    public function setPower(int $power)
    {
        $this->power = $power;

        return $this;
    }

    /**
     * @param Position $position
     * @return Character
     */
    public function setPosition(Position $position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return Position
     */
    public function getPosition() : Position
    {
        return $this->position;
    }
}