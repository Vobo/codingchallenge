<?php

declare(strict_types=1);

namespace RPGBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Immutable VO
 *
 * @ORM\Embeddable
 * @Serializer\ExclusionPolicy("all")
 */
class Position
{
    /**
     * @var int
     *
     * @ORM\Column(name="x", type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Type("integer")
     */
    protected $x;

    /**
     * @var int
     *
     * @ORM\Column(name="y", type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Type("integer")
     */
    protected $y;

    /**
     * @param int $x X >= 0
     * @param int $y Y >= 0
     */
    public function __construct(int $x, int $y)
    {
        if ($x < 0) {
            throw new \InvalidArgumentException('Min value for X is 0');
        }

        if ($y < 0) {
            throw new \InvalidArgumentException('Min value for Y is 0');
        }

        $this->x = $x;
        $this->y = $y;
    }

    /**
     * @return int
     */
    public function getX(): int
    {
        return $this->x;
    }

    /**
     * @return int
     */
    public function getY(): int
    {
        return $this->y;
    }

    /**
     * @param Position $other
     * @return bool
     */
    public function equals(self $other) : bool
    {
        return $this->getX() === $other->getX()
            && $this->getY() === $other->getY();
    }

    /**
     * @param Position $other
     * @return int
     */
    public function distance(self $other) : int
    {
        $distance = sqrt(
            ($this->getX() - $other->getX()) ** 2
            + ($this->getY() - $other->getY()) ** 2
        );

        return (int) round($distance);
    }

    /**
     * @inheritdoc
     */
    public function __toString()
    {
        return sprintf(
            'x: %d, y: %d',
            $this->x,
            $this->y
        );
    }
}