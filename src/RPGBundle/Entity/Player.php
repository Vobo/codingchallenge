<?php

declare(strict_types=1);

namespace RPGBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Table(name="player")
 * @ORM\Entity()
 *
 * @Serializer\ExclusionPolicy("all")
 */
class Player extends Character
{
    const SLOT_HEAD = 'head';
    const SLOT_MAIN_HAND = 'main hand';
    const SLOT_OFF_HAND = 'off hand';
    const SLOT_CHEST = 'chest';
    const SLOT_LEGS = 'legs';

    /**
     * @var int
     *
     * @ORM\Column(name="experience", type="integer")
     *
     * @Serializer\Expose
     */
    protected $xp = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="money", type="integer")
     *
     * @Serializer\Expose
     */
    protected $money = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="class", type="string", length=255)
     *
     * @Serializer\Expose
     */
    protected $class = '';

    /**
     * @param integer $xp
     * @return $this
     */
    public function setXP(int $xp)
    {
        $this->xp = $xp;

        return $this;
    }

    /**
     * @return int
     */
    public function getXP() : int
    {
        return $this->xp;
    }

    /**
     * @return int
     */
    public function getMoney(): int
    {
        return $this->money;
    }

    /**
     * @param int $money
     * @return $this
     */
    public function setMoney(int $money)
    {
        $this->money = $money;

        return $this;
    }

    /**
     * @return string
     */
    public function getClass(): string
    {
        return $this->class;
    }

    /**
     * @param string $class
     * @return $this
     */
    public function setClass(string $class)
    {
        $this->class = $class;

        return $this;
    }
}

