<?php

declare(strict_types=1);

namespace RPGBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use RPGBundle\Entity\Character;

/**
 * @ORM\Entity()
 *
 * @ORM\Embeddable
 * @Serializer\ExclusionPolicy("all")
 */
class World
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\Expose
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\Column(name="width", type="integer")
     *
     * @Serializer\Expose
     */
    protected $width = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="height", type="integer")
     *
     * @Serializer\Expose
     */
    protected $height = 0;

    /**
     * @var Collection|Character[]
     *
     * @ORM\OneToMany(targetEntity="Character", mappedBy="world", cascade={"persist", "remove"})
     *
     * @Serializer\Expose
     * @Serializer\Type("ArrayCollection<RPGBundle\Entity\Character>")
     */
    protected $characters;

    public function __construct()
    {
        $this->characters = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param int $width
     * @return $this
     */
    public function setWidth(int $width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * @return int
     */
    public function getWidth() : int
    {
        return $this->width;
    }

    /**
     * @param int $height
     * @return $this
     */
    public function setHeight(int $height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * @return int
     */
    public function getHeight() : int
    {
        return $this->height;
    }

    /**
     * @param Character $character
     * @return World
     * @throws \Exception When trying to add a character already in the world
     */
    public function addCharacter(Character $character)
    {
        if ($this->characters->contains($character)) {
            throw new \Exception('Character already in this world');
        }

        $this->characters->add($character);
        $character->setWorld($this);

        return $this;
    }

    /**
     * @param Position $position
     * @return bool
     */
    public function hasCharacter(Position $position) : bool
    {
        foreach ($this->characters as $character) {
            if ($position->equals($character->getPosition())) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return Character[]
     */
    public function getCharacters() : array
    {
        return $this->characters->toArray();
    }
}