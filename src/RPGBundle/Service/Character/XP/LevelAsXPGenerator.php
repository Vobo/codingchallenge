<?php

declare(strict_types=1);

namespace RPGBundle\Service\Character\XP;

use RPGBundle\Entity\Character;

class LevelAsXPGenerator implements XPGeneratorInterface
{
    /**
     * @inheritdoc
     */
    public function generateXP(Character $character): int
    {
        return $character->getLevel();
    }
}