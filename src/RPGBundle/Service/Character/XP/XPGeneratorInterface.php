<?php

declare(strict_types=1);

namespace RPGBundle\Service\Character\XP;

use RPGBundle\Entity\Character;

interface XPGeneratorInterface
{
    /**
     * @param Character $character
     * @return int
     */
    public function generateXP(Character $character) : int;
}