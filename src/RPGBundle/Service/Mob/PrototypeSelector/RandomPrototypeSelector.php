<?php

declare(strict_types=1);

namespace RPGBundle\Service\Mob\PrototypeSelector;

use RPGBundle\Entity\Mob;

class RandomPrototypeSelector implements PrototypeSelectorInterface
{
    /**
     * @inheritdoc
     */
    public function selectPrototype(array $prototypes): Mob
    {
        if (!$prototypes) {
            throw new \InvalidArgumentException('Expected at least 1 prototype to choose from');
        }

        $key = random_int(0, count($prototypes) - 1);

        return $prototypes[$key];
    }
}