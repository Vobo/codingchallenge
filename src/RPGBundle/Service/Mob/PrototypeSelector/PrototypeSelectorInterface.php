<?php

declare(strict_types=1);

namespace RPGBundle\Service\Mob\PrototypeSelector;

use RPGBundle\Entity\Mob;

interface PrototypeSelectorInterface
{
    /**
     * @param Mob[] $prototypes
     * @return Mob
     */
    public function selectPrototype(array $prototypes) : Mob;
}