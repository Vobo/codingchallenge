<?php

declare(strict_types=1);

namespace RPGBundle\Service\Mob\AI;

use RPGBundle\Bus\BusInterface;
use RPGBundle\Command\CharacterMove;
use RPGBundle\Entity\Mob;
use RPGBundle\Entity\Position;
use RPGBundle\Handler\InvalidActionException;
use RPGBundle\Service\World\WorldContextInterface;

class MoveTowardsPlayer implements AIInterface
{
    /**
     * @var BusInterface
     */
    protected $commandBus;

    /**
     * @var WorldContextInterface
     */
    protected $worldContext;

    public function __construct(
        BusInterface $commandBus,
        WorldContextInterface $worldContext
    ) {
        $this->commandBus = $commandBus;
        $this->worldContext = $worldContext;
    }

    /**
     * @param Mob $mob
     * @return bool
     * @throws \Exception
     */
    public function takeAction(Mob $mob): bool
    {
        // Move towards player
        $playerPosition = $this->worldContext->getPlayer()->getPosition();
        $mobPosition = $mob->getPosition();

        $x = $playerPosition->getX() - $mobPosition->getX();
        $x = 0 !== $x ? $x / abs($x) : 0;
        $x = $mobPosition->getX() + $x;

        $y = $playerPosition->getY() - $mobPosition->getY();
        $y = 0 !== $y ? $y / abs($y) : 0;
        $y = $mobPosition->getY() + $y;

        $newPosition = new Position($x, $y);

        if ($newPosition->equals($mobPosition)) {
            return false; // Not moving
        }

        $command = new CharacterMove(
            $mob,
            $newPosition
        );

        try {
            $this->commandBus->dispatch($command);

            return true;
        } catch (InvalidActionException $e) {
            return false; // Space taken - skip moving altogether
        }
    }
}