<?php

declare(strict_types=1);

namespace RPGBundle\Service\Mob\AI;

use RPGBundle\Entity\Mob;

interface AIInterface
{
    /**
     * @param Mob $mob
     * @return bool Whether any action took place
     */
    public function takeAction(Mob $mob) : bool;
}