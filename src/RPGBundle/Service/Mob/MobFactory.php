<?php

declare(strict_types=1);

namespace RPGBundle\Service\Mob;

use RPGBundle\Entity\Mob;
use RPGBundle\Service\Mob\Modifier\ModifierInterface;
use RPGBundle\Service\Mob\PrototypeSelector\PrototypeSelectorInterface;

class MobFactory implements MobFactoryInterface
{
    /**
     * @var PrototypeSelectorInterface
     */
    protected $prototypeSelector;

    /**
     * @var Mob[]
     */
    protected $prototypes = [];

    /**
     * @var ModifierInterface[]
     */
    protected $modifiers = [];

    public function __construct(PrototypeSelectorInterface $prototypeSelector)
    {
        $this->prototypeSelector = $prototypeSelector;
    }

    /**
     * @param Mob $prototype
     * @return MobFactory
     */
    public function registerPrototype(Mob $prototype)
    {
        $this->prototypes[] = $prototype;

        return $this;
    }

    /**
     * @param ModifierInterface $modifier
     * @return $this
     */
    public function registerModifier(ModifierInterface $modifier)
    {
        $this->modifiers[] = $modifier;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function createMob(): Mob
    {
        if (!$this->prototypes) {
            throw new \Exception('Register prototypes first');
        }

        $prototype = $this->prototypeSelector->selectPrototype($this->prototypes);
        $mob = clone $prototype;

        foreach ($this->modifiers as $modifier) {
            if ($modifier->supports($mob)) {
                $modifier->modify($mob);
            }
        }

        return $mob;
    }
}