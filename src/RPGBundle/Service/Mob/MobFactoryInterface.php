<?php

declare(strict_types=1);

namespace RPGBundle\Service\Mob;

use RPGBundle\Entity\Mob;

interface MobFactoryInterface
{
    /**
     * New mob ready to be put on the map
     * Still needs a position though
     *
     * @return Mob
     */
    public function createMob() : Mob;
}