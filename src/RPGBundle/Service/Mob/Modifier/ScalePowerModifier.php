<?php

declare(strict_types=1);

namespace RPGBundle\Service\Mob\Modifier;

use RPGBundle\Entity\Mob;

/**
 * Depends on mob level - add it after all level modifiers.
 */
class ScalePowerModifier implements ModifierInterface
{
    const POWER_DIVISOR = 2;

    /**
     * @inheritdoc
     */
    public function supports(Mob $mob): bool
    {
        return 0 === $mob->getPower();
    }

    /**
     * @inheritdoc
     */
    public function modify(Mob $mob)
    {
        if (0 === $mob->getLevel()) {
            throw new \Exception('Set level first');
        }

        $power = $mob->getLevel() / self::POWER_DIVISOR;
        $power = (int) max($power, 1);

        $mob->setPower($power);
    }
}