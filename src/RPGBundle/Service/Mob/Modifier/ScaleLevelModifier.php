<?php

declare(strict_types=1);

namespace RPGBundle\Service\Mob\Modifier;

use RPGBundle\Entity\Mob;
use RPGBundle\Service\World\WorldContextInterface;

class ScaleLevelModifier implements ModifierInterface
{
    const DEFAULT_PLAYER_LEVEL = 10; // Assume it's 1

    /**
     * @var WorldContextInterface
     */
    protected $worldContext;

    /**
     * @var int
     */
    protected $levelOffset;

    /**
     * @var int
     */
    protected $maxLevelDiff;

    /**
     * Mob level = Player level + $levelOffset + rand(-$maxLevelDiff, +$maxLevelDiff)
     *
     * @param WorldContextInterface $worldContext
     * @param int $levelOffset Min Mob level compared to Player level
     * @param int $maxLevelDiff Max diff (negative/positive) between offset Mob level and Player levels
     */
    public function __construct(
        WorldContextInterface $worldContext,
        int $levelOffset = -2,
        int $maxLevelDiff = 3
    ) {
        $this->worldContext = $worldContext;
        $this->levelOffset = $levelOffset;
        $this->maxLevelDiff = $maxLevelDiff;
    }

    /**
     * @inheritdoc
     */
    public function supports(Mob $mob): bool
    {
        return 0 === $mob->getLevel();
    }

    /**
     * @inheritdoc
     */
    public function modify(Mob $mob)
    {
        $playerLevel = $this->worldContext->hasWorld()
            ? $this->worldContext->getPlayer()->getLevel()
            : self::DEFAULT_PLAYER_LEVEL;

        if (0 === $playerLevel) {
            throw new \Exception('Set player level first');
        }

        $baseLevel = $playerLevel + $this->levelOffset;
        $level = $baseLevel + random_int(-$this->maxLevelDiff, $this->maxLevelDiff);

        $mob->setLevel(max($level, 1));
    }
}