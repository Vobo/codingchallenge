<?php

declare(strict_types=1);

namespace RPGBundle\Service\Mob\Modifier;

use RPGBundle\Entity\Mob;

interface ModifierInterface
{
    /**
     * @param Mob $mob
     * @return bool
     */
    public function supports(Mob $mob) : bool;

    /**
     * @param Mob $mob
     * @return void
     */
    public function modify(Mob $mob);
}