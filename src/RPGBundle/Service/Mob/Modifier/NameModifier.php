<?php

declare(strict_types=1);

namespace RPGBundle\Service\Mob\Modifier;

use Faker\Generator;
use RPGBundle\Entity\Mob;

class NameModifier implements ModifierInterface
{
    /**
     * @var Generator
     */
    protected $faker;

    public function __construct(Generator $faker)
    {
        $this->faker = $faker;
    }

    /**
     * @inheritdoc
     */
    public function supports(Mob $mob): bool
    {
        return '' === $mob->getName();
    }

    /**
     * @inheritdoc
     */
    public function modify(Mob $mob)
    {
        $mob->setName($this->faker->name);
    }
}