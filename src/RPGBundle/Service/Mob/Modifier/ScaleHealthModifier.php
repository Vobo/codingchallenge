<?php

declare(strict_types=1);

namespace RPGBundle\Service\Mob\Modifier;

use RPGBundle\Entity\Mob;

/**
 * Depends on mob level - add it after all level modifiers.
 */
class ScaleHealthModifier implements ModifierInterface
{
    const HEALTH_MULTIPLIER = 10;

    /**
     * @inheritdoc
     */
    public function supports(Mob $mob): bool
    {
        return 0 === $mob->getHealth();
    }

    /**
     * @inheritdoc
     */
    public function modify(Mob $mob)
    {
        if (0 === $mob->getLevel()) {
            throw new \Exception('Set level first');
        }

        $health = $mob->getLevel() * self::HEALTH_MULTIPLIER;

        $mob->setHealth($health);
    }
}