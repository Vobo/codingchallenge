<?php

declare(strict_types=1);

namespace RPGBundle\Service\Player;

use RPGBundle\Entity\Player;

class PlayerFactory implements PlayerFactoryInterface
{
    const DEFAULT_HEALTH = 100;
    const DEFAULT_LEVEL = 1;
    const DEFAULT_XP = 0;
    const DEFAULT_POWER = 2;
    const DEFAULT_MONEY = 1000;

    /**
     * @inheritdoc
     * @todo refactor classes - each one as a separate object having unique properties
     */
    public function createPlayer(string $name, string $class): Player
    {
        if (!in_array($class, [self::CLASS_MAGE, self::CLASS_ROGUE, self::CLASS_WARRIOR])) {
            throw new \InvalidArgumentException('Invalid class, got ' . $class);
        }

        $player = new Player();
        $player
            ->setName($name)
            ->setClass($class)
            ->setHealth(self::DEFAULT_HEALTH)
            ->setLevel(self::DEFAULT_LEVEL)
            ->setXP(self::DEFAULT_XP)
            ->setPower(self::DEFAULT_POWER)
            ->setMoney(self::DEFAULT_MONEY);

        return $player;
    }
}