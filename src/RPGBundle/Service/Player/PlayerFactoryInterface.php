<?php

declare(strict_types=1);

namespace RPGBundle\Service\Player;

use RPGBundle\Entity\Player;

interface PlayerFactoryInterface
{
    const CLASS_WARRIOR = 'warrior';
    const CLASS_MAGE = 'mage';
    const CLASS_ROGUE = 'rogue';

    /**
     * @param string $name
     * @param string $class
     * @return Player
     */
    public function createPlayer(string $name, string $class) : Player;
}