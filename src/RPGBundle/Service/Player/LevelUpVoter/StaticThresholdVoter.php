<?php

declare(strict_types=1);

namespace RPGBundle\Service\Player\LevelUpVoter;

use RPGBundle\Entity\Player;

class StaticThresholdVoter implements LevelUpVoterInterface
{
    /**
     * @var int
     */
    protected $threshold;

    /**
     * @param int $threshold XP required for level up
     */
    public function __construct(
        int $threshold = 20
    ) {
        if ($threshold < 1) {
            throw new \InvalidArgumentException('Threshold too low');
        }

        $this->threshold = $threshold;
    }

    /**
     * @inheritdoc
     */
    public function requiredXP(Player $player): int
    {
        return $this->threshold;
    }
}