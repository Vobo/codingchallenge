<?php

declare(strict_types=1);

namespace RPGBundle\Service\Player\LevelUpVoter;

use RPGBundle\Entity\Player;

class ThresholdScaledWithLevelVoter implements LevelUpVoterInterface
{
    /**
     * @var float
     */
    protected $levelMultiplier;

    /**
     * @param float $levelMultiplier XP required for level up = player level * $levelMultiplier
     */
    public function __construct(float $levelMultiplier = 10)
    {
        $this->levelMultiplier = $levelMultiplier;
    }

    /**
     * @inheritdoc
     */
    public function requiredXP(Player $player): int
    {
        return (int) round($player->getLevel() * $this->levelMultiplier);
    }
}
