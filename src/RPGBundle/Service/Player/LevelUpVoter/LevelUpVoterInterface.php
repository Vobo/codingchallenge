<?php

declare(strict_types=1);

namespace RPGBundle\Service\Player\LevelUpVoter;

use RPGBundle\Entity\Player;

interface LevelUpVoterInterface
{
    /**
     * @param Player $player
     * @return int XP required for next level
     */
    public function requiredXP(Player $player) : int;
}