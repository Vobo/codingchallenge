<?php

declare(strict_types=1);

namespace RPGBundle\Service\World;

use RPGBundle\Entity\World;

interface WorldBuilderFactoryInterface
{
    /**
     * @return WorldBuilderInterface
     */
    public function create() : WorldBuilderInterface;

    /**
     * @param World $world
     * @return WorldBuilderInterface
     */
    public function createForWorld(World $world) : WorldBuilderInterface;
}