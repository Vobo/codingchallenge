<?php

declare(strict_types=1);

namespace RPGBundle\Service\World;

use RPGBundle\Entity\Character;
use RPGBundle\Entity\Mob;
use RPGBundle\Entity\Npc;
use RPGBundle\Entity\Player;
use RPGBundle\Entity\World;
use RPGBundle\Service\World\PositionGenerator\PositionGeneratorInterface;

class WorldBuilder implements WorldBuilderInterface
{
    /**
     * @var PositionGeneratorInterface
     */
    protected $mobPositionGenerator;

    /**
     * @var PositionGeneratorInterface
     */
    protected $playerPositionGenerator;

    /**
     * @var PositionGeneratorInterface
     */
    protected $npcPositionGenerator;

    /**
     * @var World
     */
    protected $world;

    /**
     * Flag
     *
     * @var bool
     */
    protected $hasPlayer;

    /**
     * Flag
     *
     * @var bool
     */
    protected $hasMobs;

    public function __construct(
        PositionGeneratorInterface $mobPositionGenarator,
        PositionGeneratorInterface $playerPositionGenerator,
        PositionGeneratorInterface $npcPositionGenerator,
        World $world,
        bool $hasPlayer = false,
        bool $hasMobs = false
    ) {
        $this->mobPositionGenerator = $mobPositionGenarator;
        $this->playerPositionGenerator = $playerPositionGenerator;
        $this->npcPositionGenerator = $npcPositionGenerator;
        $this->world = $world;
        $this->hasPlayer = $hasPlayer;
        $this->hasMobs = $hasMobs;
    }

    /**
     * @inheritdoc
     */
    public function setDimensions(int $width, int $height)
    {
        if (0 !== $this->world->getWidth()) {
            throw new \Exception('Dimensions set already');
        }

        $this->world
            ->setWidth($width)
            ->setHeight($height);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function addMob(Mob $mob)
    {
        $this->addCharacter($this->mobPositionGenerator, $mob);
        $this->hasMobs = true;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function addPlayer(Player $player)
    {
        $this->addCharacter($this->playerPositionGenerator, $player);
        $this->hasPlayer = true;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function addNpc(Npc $npc)
    {
        $this->addCharacter($this->npcPositionGenerator, $npc);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getWorld() : World
    {
        if (!$this->hasPlayer) {
            throw new \Exception('Add player first');
        }

        if (!$this->hasMobs) {
            throw new \Exception('Add mobs first');
        }

        return $this->world;
    }

    /**
     * @param PositionGeneratorInterface $positionGenerator
     * @param Character $character
     * @return void
     * @throws \Exception
     */
    protected function addCharacter(PositionGeneratorInterface $positionGenerator, Character $character)
    {
        $position = $positionGenerator->generatePosition($this->world, $character);
        $character->setPosition($position);
        $this->world->addCharacter($character);
    }
}
