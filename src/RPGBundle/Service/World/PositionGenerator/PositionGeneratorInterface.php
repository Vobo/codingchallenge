<?php

declare(strict_types=1);

namespace RPGBundle\Service\World\PositionGenerator;

use RPGBundle\Entity\Character;
use RPGBundle\Entity\Position;
use RPGBundle\Entity\World;

interface PositionGeneratorInterface
{
    /**
     * @param World $world
     * @param Character $character
     * @return Position Position to be used by new Character
     */
    public function generatePosition(World $world, Character $character) : Position;
}