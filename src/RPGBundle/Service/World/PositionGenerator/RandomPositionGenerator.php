<?php

declare(strict_types=1);

namespace RPGBundle\Service\World\PositionGenerator;

use RPGBundle\Entity\Character;
use RPGBundle\Entity\Position;
use RPGBundle\Entity\World;

class RandomPositionGenerator implements PositionGeneratorInterface
{
    const MAX_TRIES = 10;

    /**
     * @inheritdoc
     */
    public function generatePosition(World $world, Character $character): Position
    {
        for ($i = 0; $i < self::MAX_TRIES; $i++) {
            $position = new Position($this->getRandomX($world), $this->getRandomY($world));

            if (!$world->hasCharacter($position)) {
                return $position;
            }
        }

        throw new \Exception(sprintf(
            'Could not find empty position in the world, tried for %d times',
            self::MAX_TRIES
        ));
    }

    /**
     * @param World $world
     * @return int
     */
    protected function getRandomX(World $world) : int
    {
        return random_int(0, $world->getWidth() - 1);
    }

    /**
     * @param World $world
     * @return int
     */
    protected function getRandomY(World $world) : int
    {
        return random_int(0, $world->getHeight() - 1);
    }
}