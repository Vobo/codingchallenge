<?php

declare(strict_types=1);

namespace RPGBundle\Service\World;

use RPGBundle\Entity\Mob;
use RPGBundle\Entity\Npc;
use RPGBundle\Entity\Player;
use RPGBundle\Entity\World;

interface WorldBuilderInterface
{
    /**
     * @param int $width
     * @param int $height
     * @return $this
     * @throws \Exception When trying to change dimensions once set
     */
    public function setDimensions(int $width, int $height);

    /**
     * @param Mob $mob
     * @return $this
     * @throws \Exception When trying to add same mob multiple times
     */
    public function addMob(Mob $mob);

    /**
     * @param Player $player
     * @return $this
     * @throws \Exception When another player was added already
     */
    public function addPlayer(Player $player);

    /**
     * @param Npc $npc
     * @return $this
     * @throws \Exception When trying to add same npc multiple times
     */
    public function addNpc(Npc $npc);

    /**
     * @return World
     */
    public function getWorld() : World;
}