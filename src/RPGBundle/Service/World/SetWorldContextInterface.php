<?php

declare(strict_types=1);

namespace RPGBundle\Service\World;

use RPGBundle\Entity\World;

interface SetWorldContextInterface
{
    /**
     * @param World $world
     * @return $this
     */
    public function setWorld(World $world);
}