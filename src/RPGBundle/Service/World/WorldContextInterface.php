<?php

declare(strict_types=1);

namespace RPGBundle\Service\World;

use RPGBundle\Entity\Character;
use RPGBundle\Entity\Mob;
use RPGBundle\Entity\Npc;
use RPGBundle\Entity\Player;
use RPGBundle\Entity\Position;
use RPGBundle\Entity\World;

interface WorldContextInterface
{
    /**
     * Whether World is set or not
     *
     * @return bool
     */
    public function hasWorld() : bool;

    /**
     * @return World
     * @throws \Exception
     */
    public function getWorld() : World;

    /**
     * @return Player
     * @throws \Exception
     */
    public function getPlayer() : Player;

    /**
     * @return bool
     * @throws \Exception
     */
    public function hasPlayer() : bool;

    /**
     * @return Mob[]
     * @throws \Exception
     */
    public function getMobs() : array;

    /**
     * @return bool
     * @throws \Exception
     */
    public function hasMobs() : bool;

    /**
     * @return Npc[]
     * @throws \Exception
     */
    public function getNpcs() : array;

    /**
     * @param Position $position
     * @return Character
     * @throws \Exception
     */
    public function get(Position $position) : Character;

    /**
     * @param int $distance
     * @return Character[]
     * @throws \Exception
     */
    public function getInDistanceFromPlayer(int $distance) : array;
}