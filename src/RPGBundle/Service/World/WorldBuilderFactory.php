<?php

declare(strict_types=1);

namespace RPGBundle\Service\World;

use RPGBundle\Entity\World;
use RPGBundle\Service\World\PositionGenerator\PositionGeneratorInterface;

class WorldBuilderFactory implements WorldBuilderFactoryInterface
{
    /**
     * @var PositionGeneratorInterface
     */
    protected $mobPositionGenerator;

    /**
     * @var PositionGeneratorInterface
     */
    protected $playerPositionGenerator;

    /**
     * @var PositionGeneratorInterface
     */
    protected $npcPositionGenerator;

    public function __construct(
        PositionGeneratorInterface $mobPositionGenarator,
        PositionGeneratorInterface $playerPositionGenerator,
        PositionGeneratorInterface $npcPositionGenerator
    ) {
        $this->mobPositionGenerator = $mobPositionGenarator;
        $this->playerPositionGenerator = $playerPositionGenerator;
        $this->npcPositionGenerator = $npcPositionGenerator;
    }

    /**
     * @inheritdoc
     */
    public function create(): WorldBuilderInterface
    {
        return new WorldBuilder(
            $this->mobPositionGenerator,
            $this->playerPositionGenerator,
            $this->npcPositionGenerator,
            new World()
        );
    }

    /**
     * @inheritdoc
     */
    public function createForWorld(World $world): WorldBuilderInterface
    {
        $worldContext = new WorldContext($world);

        return new WorldBuilder(
            $this->mobPositionGenerator,
            $this->playerPositionGenerator,
            $this->npcPositionGenerator,
            $world,
            $worldContext->hasPlayer(),
            $worldContext->hasMobs()
        );
    }
}
