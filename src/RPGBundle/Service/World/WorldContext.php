<?php

declare(strict_types=1);

namespace RPGBundle\Service\World;

use RPGBundle\Entity\Character;
use RPGBundle\Entity\Mob;
use RPGBundle\Entity\Npc;
use RPGBundle\Entity\Player;
use RPGBundle\Entity\Position;
use RPGBundle\Entity\World;

class WorldContext implements WorldContextInterface, SetWorldContextInterface
{
    /**
     * @var World|null
     */
    protected $world;

    public function __construct(World $world = null)
    {
        $this->world = $world;
    }

    /**
     * @inheritdoc
     */
    public function setWorld(World $world)
    {
        $this->world = $world;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function hasWorld() : bool
    {
        return (bool) $this->world;
    }

    /**
     * @inheritdoc
     */
    public function getWorld() : World
    {
        $this->assertHasWorld();

        return $this->world;
    }

    /**
     * @inheritdoc
     */
    public function hasPlayer() : bool
    {
        $this->assertHasWorld();

        try {
            $this->getPlayer();

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function getPlayer() : Player
    {
        $this->assertHasWorld();

        foreach ($this->world->getCharacters() as $character) {
            if ($character instanceof Player) {
                return $character;
            }
        }

        throw new \Exception('No players in this world');
    }

    /**
     * @inheritdoc
     */
    public function hasMobs(): bool
    {
        $this->assertHasWorld();

        foreach ($this->world->getCharacters() as $character) {
            if ($character instanceof Mob) {
                return true;
            }
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function getMobs() : array
    {
        $this->assertHasWorld();

        $mobs = [];

        foreach ($this->world->getCharacters() as $character) {
            if ($character instanceof Mob) {
                $mobs[] = $character;
            }
        }

        return $mobs;
    }

    /**
     * @inheritdoc
     */
    public function getNpcs() : array
    {
        $this->assertHasWorld();

        $npcs = [];

        foreach ($this->world->getCharacters() as $character) {
            if ($character instanceof Npc) {
                $npcs[] = $character;
            }
        }

        return $npcs;
    }

    /**
     * @inheritdoc
     */
    public function get(Position $position): Character
    {
        $this->assertHasWorld();

        foreach ($this->world->getCharacters() as $character) {
            if ($character->getPosition()->equals($position)) {
                return $character;
            }
        }

        throw new \Exception('No character at positon ' . $position);
    }


    /**
     * @inheritdoc
     */
    public function getInDistanceFromPlayer(int $distance) : array
    {
        $this->assertHasWorld();

        $position = $this->getPlayer()->getPosition();
        $filtered = [];

        foreach ($this->world->getCharacters() as $character) {
            if ($character->getPosition()->distance($position) <= $distance) {
                $filtered[] = $character;
            }
        }

        return $filtered;
    }

    /**
     * @throws \Exception
     */
    protected function assertHasWorld()
    {
        if (!$this->hasWorld()) {
            throw new \Exception('Set the world first');
        }
    }
}
