<?php

declare(strict_types=1);

namespace RPGBundle\Service;

class Dice
{
    /**
     * @var int
     */
    protected $sides;

    public function __construct(int $sides)
    {
        if ($sides <= 1) {
            throw new \InvalidArgumentException('Dice must have at least 2 sides');
        }

        $this->sides = $sides;
    }

    /**
     * @return int Between 1 and number of sides, inclusive
     */
    public function roll() : int
    {
        return random_int(1, $this->sides);
    }
}