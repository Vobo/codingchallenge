<?php

declare(strict_types=1);

namespace RPGBundle\Service;

use RPGBundle\Entity\Item;

class Container
{
    /**
     * @var Item[]
     */
    protected $items = [];

    public function __construct(array $items)
    {
        foreach($items as $item) {
            $this->addItem($item);
        }
    }

    /**
     * @return Item[]
     */
    public function getItems() : array
    {
        return $this->items;
    }

    /**
     * @param Item $item
     * @return $this
     */
    protected function addItem(Item $item)
    {
        $this->items[] = $item;

        return $this;
    }
}