<?php

declare(strict_types=1);

namespace RPGBundle\Bus;

use RPGBundle\Command\CommandInterface;

interface BusInterface
{
    /**
     * @param CommandInterface $command
     * @return mixed Command result
     */
    public function dispatch(CommandInterface $command);
}