<?php

declare(strict_types=1);

namespace RPGBundle\Bus;

use Psr\Container\ContainerInterface;
use RPGBundle\Command\CommandInterface;
use RPGBundle\Handler\HandlerInterface;

class CommandBus implements BusInterface
{
    /**
     * @var ContainerInterface
     */
    protected $handlerRegistry;

    public function __construct(
        ContainerInterface $handlerRegistry
    ) {
        $this->handlerRegistry = $handlerRegistry;
    }

    /**
     * @inheritdoc
     */
    public function dispatch(CommandInterface $command)
    {
        $commandClass = get_class($command);

        if (!$this->handlerRegistry->has($commandClass)) {
            throw  new \Exception('Could not find a handler for command ' . get_class($command));
        }

        $handler = $this->handlerRegistry->get($commandClass);

        if (!$handler instanceof HandlerInterface) {
            throw new \Exception('Handler should implement HandlerInterface, got ' . get_class($handler));
        }

        return $handler->handle($command);
    }
}