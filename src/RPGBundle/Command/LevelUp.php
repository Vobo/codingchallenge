<?php

declare(strict_types=1);

namespace RPGBundle\Command;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

use RPGBundle\Entity\Player;

/**
 * @Serializer\ExclusionPolicy("all")
 */
class LevelUp implements CommandInterface
{
    const IMPROVE_POWER = 'power';
    const IMPROVE_HEALTH = 'health';

    /**
     * @var Player
     */
    protected $player;

    /**
     * @var string
     *
     * @Serializer\Expose
     * @Serializer\Type("string")
     *
     * @Assert\NotBlank
     * @Assert\Choice(choices={"power", "health"}, message="Valid options are: power, health")
     */
    protected $improve;

    /**
     * @param Player $player
     * @return $this
     */
    public function setPlayer(Player $player)
    {
        $this->player = $player;

        return $this;
    }

    /**
     * @return Player
     */
    public function getPlayer() : Player
    {
        return $this->player;
    }

    /**
     * @return string
     */
    public function getImprove() : string
    {
        return $this->improve;
    }
}