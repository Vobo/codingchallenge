<?php

declare(strict_types=1);

namespace RPGBundle\Command;

use RPGBundle\Entity\Character;

/**
 * Action considered to take a whole turn
 */
interface TurnCommandInterface extends CommandInterface
{
    /**
     * @return Character Character taking action
     */
    public function getCharacter() : Character;
}