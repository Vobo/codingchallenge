<?php

declare(strict_types=1);

namespace RPGBundle\Command;

use RPGBundle\Entity\Character;
use RPGBundle\Entity\Position;

class CharacterAttack implements TurnCommandInterface
{
    /**
     * @var Character
     */
    protected $character;

    /**
     * @var Position
     */
    protected $target;

    public function __construct(Character $character, Position $target)
    {
        $this->character = $character;
        $this->target = $target;
    }

    /**
     * @inheritdoc
     */
    public function getCharacter() : Character
    {
        return $this->character;
    }

    /**
     * @return Position
     */
    public function getTarget() : Position
    {
        return $this->target;
    }
}