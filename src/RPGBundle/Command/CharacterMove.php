<?php

declare(strict_types=1);

namespace RPGBundle\Command;

use RPGBundle\Entity\Character;
use RPGBundle\Entity\Position;

class CharacterMove implements TurnCommandInterface
{
    /**
     * @var Character
     */
    protected $character;

    /**
     * @var Position
     */
    protected $position;

    public function __construct(Character $character, Position $targetPosition)
    {
        $this->character = $character;
        $this->position = $targetPosition;
    }

    /**
     * @inheritdoc
     */
    public function getCharacter() : Character
    {
        return $this->character;
    }

    /**
     * @return Position
     */
    public function getPosition() : Position
    {
        return $this->position;
    }
}