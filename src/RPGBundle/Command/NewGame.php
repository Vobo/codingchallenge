<?php

declare(strict_types=1);

namespace RPGBundle\Command;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @Serializer\ExclusionPolicy("all")
 */
class NewGame implements CommandInterface
{
    /**
     * @var int
     *
     * @Serializer\Expose
     * @Serializer\Type("integer")
     *
     * @Assert\GreaterThan(3)
     * @Assert\LessThanOrEqual(1000)
     */
    protected $width;

    /**
     * @var int
     *
     * @Serializer\Expose
     * @Serializer\Type("integer")
     *
     * @Assert\GreaterThan(3)
     * @Assert\LessThanOrEqual(1000)
     */
    protected $height;

    /**
     * @var string
     *
     * @Serializer\Expose
     * @Serializer\Type("string")
     *
     * @Assert\NotBlank
     */
    protected $name;

    /**
     * @var string
     *
     * @Serializer\Expose
     * @Serializer\Type("string")
     *
     * @Assert\NotBlank
     * @Assert\Choice(choices={"rogue", "warrior", "mage"}, message="Valid options are: rogue, warrior, mage")
     */
    protected $class;

    /**
     * @return int
     */
    public function getWidth() : int
    {
        return $this->width;
    }

    /**
     * @return int
     */
    public function getHeight() : int
    {
        return $this->height;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getClass() : string
    {
        return $this->class;
    }
}
