<?php

declare(strict_types=1);

namespace RPGBundle\Model;

use RPGBundle\Entity\Character;
use JMS\Serializer\Annotation as Serializer;

/**
 * @Serializer\ExclusionPolicy("all")
 */
class AttackResult
{
    /**
     * @var Character
     *
     * @Serializer\Expose
     * @Serializer\Type("RPGBundle\Entity\Character")
     */
    protected $attacker;

    /**
     * @var Character
     *
     * @Serializer\Expose
     * @Serializer\Type("RPGBundle\Entity\Character")
     */
    protected $defender;

    /**
     * @var int
     *
     * @Serializer\Expose
     * @Serializer\Type("integer")
     */
    protected $damage;

    /**
     * @var int
     *
     * @Serializer\Expose
     * @Serializer\Type("integer")
     */
    protected $xp;

    public function __construct(
        Character $attacker,
        Character $defender,
        int $damage,
        int $xp
    ) {
        $this->attacker = $attacker;
        $this->defender = $defender;
        $this->damage = $damage;
        $this->xp = $xp;
    }

    /**
     * @return Character
     */
    public function getAttacker() : Character
    {
        return $this->attacker;
    }

    /**
     * @return Character
     */
    public function getDefender() : Character
    {
        return $this->attacker;
    }

    /**
     * @return int
     */
    public function getDamage() : int
    {
        return $this->damage;
    }

    /**
     * @return int
     */
    public function getXp() : int
    {
        return $this->xp;
    }
}