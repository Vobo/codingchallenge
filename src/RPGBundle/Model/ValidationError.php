<?php

declare(strict_types=1);

namespace RPGBundle\Model;

use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;

class ValidationError
{
    const KEY_PROPERTY = 'property_path';
    const KEY_MESSAGE = 'message';

    /**
     * @var string
     */
    protected $message;

    /**
     * @var string
     */
    protected $property;

    public function __construct(string $message, string $property = '')
    {
        $this->message = $message;
        $this->property = $property;
    }

    /**
     * @return View
     */
    public function toView() : View
    {
        return new View($this->getData(), Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @return array
     */
    protected function getData() : array
    {
        return [
            [ self::KEY_PROPERTY => $this->property, self::KEY_MESSAGE => $this->message ]
        ];
    }
}