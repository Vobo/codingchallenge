<?php

namespace RPGBundle;

use RPGBundle\DependencyInjection\Compiler\MobFactoryPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class RPGBundle
 */
class RPGBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container
            ->addCompilerPass(new MobFactoryPass());
    }
}
