To run tests use following command (if you use Docker)

```
chmod +x bin/*
./bin/docker_phpunit
```
Or if you don't use docker:

```
./vendor/bin/phpunit
```

For code coverage, run (on Docker):
```
./bin/docker_phpunit --coverage-text 
```

Or without Docker:
```
./vendor/bin/phpunit --coverage-text 
```


Current coverage report:
```
PHPUnit 6.2.2 by Sebastian Bergmann and contributors.

......................................                            38 / 38 (100%)

Time: 327 ms, Memory: 6.00MB

OK (38 tests, 63 assertions)


Code Coverage Report:     
  2018-01-31 17:38:41     
                          
 Summary:                 
  Classes: 12.24% (6/49)  
  Methods:  9.58% (16/167)
  Lines:    5.88% (54/918)

\RPGBundle\Service::Dice
  Methods: 100.00% ( 2/ 2)   Lines: 100.00% (  5/  5)
\RPGBundle\Service\Mob::MobFactory
  Methods: 100.00% ( 4/ 4)   Lines: 100.00% ( 14/ 14)
\RPGBundle\Service\Mob\Modifier::NameModifier
  Methods: 100.00% ( 3/ 3)   Lines: 100.00% (  5/  5)
\RPGBundle\Service\Mob\Modifier::ScaleHealthModifier
  Methods: 100.00% ( 2/ 2)   Lines: 100.00% (  6/  6)
\RPGBundle\Service\Mob\Modifier::ScaleLevelModifier
  Methods:  66.67% ( 2/ 3)   Lines:  92.86% ( 13/ 14)
\RPGBundle\Service\Mob\Modifier::ScalePowerModifier
  Methods: 100.00% ( 2/ 2)   Lines: 100.00% (  7/  7)
\RPGBundle\Service\Mob\PrototypeSelector::RandomPrototypeSelector
  Methods: 100.00% ( 1/ 1)   Lines: 100.00% (  4/  4)
```

Covers Mob-related services.

Behat tests - not provided, I already took longer than I was supposed to.