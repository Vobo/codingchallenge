# Solution description

### How to play

Once you start the application you will be taken to API documentation. It's split into two parts:
- methods - list of API methods provided by the application,
- models - list of models consumed and returned by the application.

To start, start a new game - `POST /game`. Set the map dimensions, pass your character's name and class.
In the response you will be provided a world id, which is used in all subsequent API calls, as well as a map overview.

Now you can move around, attack mobs and level up to your heart's content. 

### Framework changes:
For some reason, production endpoint was removed and replaced with dev one. I have restored the prod endpoint, moved
dev one to another file that's additionally secured from accessing remotely.

Documentation relies heavily on NelmioApiDocBundle and Swagger annotations. Took a while to configure and get around
Swagger, but the result makes up for the troubles. There are still some minor quirks for tackle though.

### Missing features:
 - save / load
 - mobs don't attack, just swarm towards the player

### Things to fix:
 - dead code - Item abstraction, NPCs, classes,
 - missing logger - would be nice to know who moved where, who damaged who for how much, how stats updated during level up,
 - some models described in documentation are missing some properties - ie. while the API returns all objects sharing Character class,
 - add Doctrine fixtures, store Mob prototypes in DB,
 each one of them can have additional properties not shared with the common class; these won't show up right now,
 - naming things - LevelUpVoter no longer reflects, what is it's real role,
 - parts of the code need additional abstraction, due to time constraints these were just hardcoded and marked with `@todo` comments,
 - player doesn't know when he can level up,
 - there is no 'max health', only current health; since mobs don't attack player and cannot level up, this doesn't cause any bugs.
  