#Coding Challenge



Please read the following docs:
- [Introduction to the challenge](docs/challenge.md)
- [Installing the application](docs/install.md)
- [About tests](docs/tests.md)
- [Description](docs/description.md)
