<?php

declare(strict_types=1);

namespace Tests\RPGBundle\Service\Mob\PrototypeSelector;

use PHPUnit\Framework\TestCase;
use RPGBundle\Entity\Mob;
use RPGBundle\Service\Mob\PrototypeSelector\RandomPrototypeSelector;

class RandomPrototypeSelectorTest extends TestCase
{
    /**
     * @var RandomPrototypeSelector
     */
    protected $selector;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Mob
     */
    protected $prototype;

    public function setUp()
    {
        $this->selector = new RandomPrototypeSelector();

        $this->prototype = $this
            ->getMockBuilder(Mob::class)
            ->getMock();
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testFailsWhenEmptyArrayPassed()
    {
        $this->selector->selectPrototype([]);
    }

    public function testReturnsPassedPrototype()
    {
        $selected = $this->selector->selectPrototype([$this->prototype]);

        $this->assertSame($this->prototype, $selected);
    }

    public function testReturnsRandom()
    {
        $prototypeA = $this->prototype;
        $prototypeB = clone $prototypeA;
        $prototypeC = clone $prototypeA;
        $array = [$prototypeA, $prototypeB, $prototypeC];

        for($i = 0; $i < 20; $i++) {
            $selected = $this->selector->selectPrototype($array);

            if ($selected === $prototypeA) {
                $prototypeA = null;
            }

            if ($selected === $prototypeB) {
                $prototypeB = null;
            }

            if ($selected === $prototypeC) {
                $prototypeC = null;
            }

            if ($prototypeA === null
                && $prototypeB === null
                && $prototypeC === null
            ) {
                break;
            }
        }

        $this->assertNull($prototypeA);
        $this->assertNull($prototypeB);
        $this->assertNull($prototypeC);
    }
}