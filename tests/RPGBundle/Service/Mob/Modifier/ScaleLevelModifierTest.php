<?php

declare(strict_types=1);

namespace Tests\RPGBundle\Service\Mob\Modifier;

use PHPUnit\Framework\TestCase;
use RPGBundle\Entity\Mob;
use RPGBundle\Entity\Player;
use RPGBundle\Service\Mob\Modifier\ScaleLevelModifier;
use RPGBundle\Service\World\WorldContextInterface;

class ScaleLevelModifierTest extends TestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Mob
     */
    protected $mob;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Player
     */
    protected $player;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|WorldContextInterface
     */
    protected $worldContext;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|WorldContextInterface
     */
    protected $wordlessContext;

    /**
     * @var ScaleLevelModifier
     */
    protected $modifierWithWorld;

    /**
     * @var ScaleLevelModifier
     */
    protected $modifierWithoutWorld;

    public function setUp()
    {
        $this->mob = $this
            ->getMockBuilder(Mob::class)
            ->setMethods(['getLevel', 'setLevel'])
            ->getMock();

        $this->player = $this
            ->getMockBuilder(Player::class)
            ->setMethods(['getLevel'])
            ->getMock();

        $this->worldContext = $this
            ->getMockBuilder(WorldContextInterface::class)
            ->getMock();

        $this->worldContext
            ->expects($this->any())
            ->method('hasWorld')
            ->willReturn(true);

        $this->worldContext
            ->expects($this->any())
            ->method('getPlayer')
            ->willReturn($this->player);

        $this->wordlessContext = $this
            ->getMockBuilder(WorldContextInterface::class)
            ->getMock();

        $this->wordlessContext
            ->expects($this->any())
            ->method('hasWorld')
            ->willReturn(false);

        $this->wordlessContext
            ->expects($this->never())
            ->method('getPlayer');

        $this->modifierWithWorld = new ScaleLevelModifier($this->worldContext);
        $this->modifierWithoutWorld = new ScaleLevelModifier($this->wordlessContext);
    }

    /**
     * Data provider
     */
    public function modifierProvider()
    {
        return [
            ['modifierWithWorld'],
            ['modifierWithoutWorld']
        ];
    }

    /**
     * @dataProvider modifierProvider
     */
    public function testSetsLevel(string $modifierVariable)
    {
        $this->player
            ->expects($this->any())
            ->method('getLevel')
            ->willReturn(1);

        $this->mob
            ->expects($this->once())
            ->method('setLevel')
            ->with($this->greaterThan(0));

        $this->{$modifierVariable}->modify($this->mob);
    }

    /**
     * @dataProvider modifierProvider
     */
    public function testSupportsOnlyWhenEmpty(string $modifierVariable)
    {
        $this->mob
            ->expects($this->any())
            ->method('getLevel')
            ->willReturn(0);

        $this->assertTrue($this->{$modifierVariable}->supports($this->mob));
    }

    /**
     * @dataProvider modifierProvider
     */
    public function testDoesNotSupportWhenNameAlreadyGiven(string $modifierVariable)
    {
        $this->mob
            ->expects($this->any())
            ->method('getLevel')
            ->willReturn(1);

        $this->assertFalse($this->{$modifierVariable}->supports($this->mob));
    }

    /**
     * Data provider
     */
    public function offsetProvider()
    {
        return [
            [10, -3, 7],
            [1, -3, 1],
            [2, -3, 1],
            [1, 0, 1],
            [1, 1, 2],
            [1, 3, 4],
            [5, 0, 5],
        ];
    }

    /**
     * @dataProvider offsetProvider
     */
    public function testOffset(int $level, int $offset, int $expectedLevel)
    {
        $modifier = new ScaleLevelModifier($this->worldContext, $offset, 0);

        $this->player
            ->expects($this->any())
            ->method('getLevel')
            ->willReturn($level);

        $this->mob
            ->expects($this->once())
            ->method('setLevel')
            ->with($expectedLevel);

        $modifier->modify($this->mob);
    }

    /**
     * Data provider
     */
    public function maxDiffProvider()
    {
        return [
            [1, 0, 1, 1],
            [1, 2, 1, 3],
            [3, 1, 2, 4],
            [3, 2, 1, 5],
        ];
    }

    /**
     * @dataProvider maxDiffProvider
     */
    public function testMaxDiff(int $level, int $maxLevelDiff, int $expectedFrom, int $expectedTo)
    {
        $modifier = new ScaleLevelModifier($this->worldContext, 0, $maxLevelDiff);

        $this->player
            ->expects($this->any())
            ->method('getLevel')
            ->willReturn($level);

        $this->mob
            ->expects($this->once())
            ->method('setLevel')
            ->with($this->callback(function (int $level) use ($expectedFrom, $expectedTo) {
                return $level >= $expectedFrom && $level <= $expectedTo;
            }));

        $modifier->modify($this->mob);
    }
}