<?php

declare(strict_types=1);

namespace Tests\RPGBundle\Service\Mob\Modifier;

use Faker\Generator;
use PHPUnit\Framework\TestCase;
use RPGBundle\Entity\Mob;
use RPGBundle\Service\Mob\Modifier\NameModifier;

class NameModifierTest extends TestCase
{
    const NAME = 'koza';

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Generator
     */
    protected $faker;

    /**
     * @var NameModifier
     */
    protected $modifier;

    public function setUp()
    {
        $this->faker = $this
            ->getMockBuilder(Generator::class)
            ->getMock();

        $this->modifier = new NameModifier($this->faker);
    }

    public function testSetsName()
    {
        $mob = $this
            ->getMockBuilder(Mob::class)
            ->setMethods(['setName'])
            ->getMock();

        $mob
            ->expects($this->once())
            ->method('setName')
            ->with($this->equalTo(self::NAME));


        $this->faker->name = self::NAME;
        $this->modifier->modify($mob);
    }

    public function testSupportsOnlyWhenEmpty()
    {
        $mob = $this
            ->getMockBuilder(Mob::class)
            ->setMethods(['getName'])
            ->getMock();

        $mob
            ->expects($this->any())
            ->method('getName')
            ->willReturn('');

        $this->assertTrue($this->modifier->supports($mob));
    }

    public function testDoesNotSupportWhenNameAlreadyGiveNTest()
    {
        $mob = $this
            ->getMockBuilder(Mob::class)
            ->setMethods(['getName'])
            ->getMock();

        $mob
            ->expects($this->once())
            ->method('getName')
            ->willReturn(self::NAME);

        $this->assertFalse($this->modifier->supports($mob));
    }
}