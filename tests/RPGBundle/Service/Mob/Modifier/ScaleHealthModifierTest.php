<?php

declare(strict_types=1);

namespace Tests\RPGBundle\Service\Mob\Modifier;

use PHPUnit\Framework\TestCase;
use RPGBundle\Entity\Mob;
use RPGBundle\Service\Mob\Modifier\ScaleHealthModifier;

class ScaleHealthModifierTest extends TestCase
{
    /**
     * @var ScaleHealthModifier
     */
    protected $modifier;

    public function setUp()
    {
        $this->modifier = new ScaleHealthModifier();
    }

    public function testSetsHealth()
    {
        $mob = $this
            ->getMockBuilder(Mob::class)
            ->setMethods(['setHealth', 'getLevel'])
            ->getMock();

        $mob
            ->expects($this->once())
            ->method('setHealth')
            ->with($this->greaterThan(0));

        $mob
            ->expects($this->any())
            ->method('getLevel')
            ->willReturn(1);


        $this->modifier->modify($mob);
    }

    public function testSupportsOnlyWhenEmpty()
    {
        $mob = $this
            ->getMockBuilder(Mob::class)
            ->setMethods(['getHealth'])
            ->getMock();

        $mob
            ->expects($this->any())
            ->method('getHealth')
            ->willReturn(0);

        $this->assertTrue($this->modifier->supports($mob));
    }

    public function testDoesNotSupportWhenHealthAlreadySetTest()
    {
        $mob = $this
            ->getMockBuilder(Mob::class)
            ->setMethods(['getHealth'])
            ->getMock();

        $mob
            ->expects($this->any())
            ->method('getHealth')
            ->willReturn(1);

        $this->assertFalse($this->modifier->supports($mob));
    }

    /**
     * @expectedException \Exception
     */
    public function testRaisesExceptionWhenLevelNotSet()
    {
        $mob = $this
            ->getMockBuilder(Mob::class)
            ->setMethods(['getLevel'])
            ->getMock();

        $mob
            ->expects($this->any())
            ->method('getLevel')
            ->willReturn(0);

        $this->modifier->modify($mob);
    }
}