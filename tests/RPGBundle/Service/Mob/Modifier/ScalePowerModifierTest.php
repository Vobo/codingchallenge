<?php

declare(strict_types=1);

namespace Tests\RPGBundle\Service\Mob\Modifier;

use PHPUnit\Framework\TestCase;
use RPGBundle\Entity\Mob;
use RPGBundle\Service\Mob\Modifier\ScalePowerModifier;

class ScalePowerModifierTest extends TestCase
{
    /**
     * @var ScalePowerModifier
     */
    protected $modifier;

    public function setUp()
    {
        $this->modifier = new ScalePowerModifier();
    }

    public function testSetsPower()
    {
        $mob = $this
            ->getMockBuilder(Mob::class)
            ->setMethods(['setPower', 'getLevel'])
            ->getMock();

        $mob
            ->expects($this->once())
            ->method('setPower')
            ->with($this->greaterThan(0));

        $mob
            ->expects($this->any())
            ->method('getLevel')
            ->willReturn(1);


        $this->modifier->modify($mob);
    }

    public function testSupportsOnlyWhenEmpty()
    {
        $mob = $this
            ->getMockBuilder(Mob::class)
            ->setMethods(['getPower'])
            ->getMock();

        $mob
            ->expects($this->any())
            ->method('getPower')
            ->willReturn(0);

        $this->assertTrue($this->modifier->supports($mob));
    }

    public function testDoesNotSupportWhenHealthAlreadySetTest()
    {
        $mob = $this
            ->getMockBuilder(Mob::class)
            ->setMethods(['getPower'])
            ->getMock();

        $mob
            ->expects($this->any())
            ->method('getPower')
            ->willReturn(1);

        $this->assertFalse($this->modifier->supports($mob));
    }

    /**
     * @expectedException \Exception
     */
    public function testRaisesExceptionWhenLevelNotSet()
    {
        $mob = $this
            ->getMockBuilder(Mob::class)
            ->setMethods(['getLevel'])
            ->getMock();

        $mob
            ->expects($this->any())
            ->method('getLevel')
            ->willReturn(0);

        $this->modifier->modify($mob);
    }
}