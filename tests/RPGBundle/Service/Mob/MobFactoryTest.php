<?php

declare(strict_types=1);

namespace Tests\RPGBundle\Service\Mob;

use PHPUnit\Framework\TestCase;
use RPGBundle\Entity\Mob;
use RPGBundle\Service\Mob\MobFactory;
use RPGBundle\Service\Mob\Modifier\ModifierInterface;
use RPGBundle\Service\Mob\PrototypeSelector\PrototypeSelectorInterface;

class MobFactoryTest extends TestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Mob
     */
    protected $prototype;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|PrototypeSelectorInterface
     */
    protected $propertySelector;

    /**
     * @var MobFactory
     */
    protected $factory;

    public function setUp()
    {
        $this->prototype = $this
            ->getMockBuilder(Mob::class)
            ->getMock();

        $this->propertySelector = $this
            ->getMockBuilder(PrototypeSelectorInterface::class)
            ->getMock();

        $this->factory = new MobFactory($this->propertySelector);
    }

    public function testCreatesMob()
    {
        $this->propertySelector
            ->expects($this->once())
            ->method('selectPrototype')
            ->with($this->equalTo([$this->prototype]))
            ->willReturn($this->prototype);

        $this->factory->registerPrototype($this->prototype);
        $mob = $this->factory->createMob();

        $this->assertInstanceOf(Mob::class, $mob);
        $this->assertNotSame($this->prototype, $mob);
    }

    /**
     * @expectedException \Exception
     */
    public function testFailsWhenNoPrototypesRegistered()
    {
        $this->factory->createMob();
    }

    public function testRunsModifiers()
    {
        $this->propertySelector
            ->expects($this->once())
            ->method('selectPrototype')
            ->with($this->equalTo([$this->prototype]))
            ->willReturn($this->prototype);

        $modifier = $this
            ->getMockBuilder(ModifierInterface::class)
            ->getMock();

        $modifier
            ->expects($this->once())
            ->method('supports')
            ->willReturn(true);

        $modifier
            ->expects($this->once())
            ->method('modify')
            ->with($this->isInstanceOf(Mob::class));

        $this->factory->registerPrototype($this->prototype);
        $this->factory->registerModifier($modifier);
        $this->factory->createMob();
    }
}