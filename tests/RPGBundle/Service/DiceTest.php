<?php

declare(strict_types=1);

namespace Tests\RPGBundle\Service;

use PHPUnit\Framework\TestCase;
use RPGBundle\Service\Dice;

class DiceTest extends TestCase
{
    /**
     * Data provider
     */
    public function invalidSidesProvider()
    {
        return [
            [-1],
            [0],
            [1],
        ];
    }

    /**
     * @expectedException \Exception
     * @dataProvider invalidSidesProvider
     */
    public function testAtLeastTwoSides(int $sides)
    {
        new Dice($sides);
    }

    public function testRandomRoll()
    {
        $a = 1;
        $b = 2;
        $c = 3;
        $dice = new Dice($c);

        for ($i = 0; $i < 10; $i++) {
            $roll = $dice->roll();

            if ($roll === $a) {
                $a = null;
            }

            if ($roll === $b) {
                $b = null;
            }

            if ($roll === $c) {
                $c = null;
            }

            if ($a === null
                && $b === null
                && $c === null
            ) {
                break;
            }
        }

        $this->assertNull($a);
        $this->assertNull($b);
        $this->assertNull($c);
    }
}